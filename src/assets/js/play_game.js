const buttons = document.querySelectorAll("button");

const output = document.getElementById("idHasil");
const bgHasil = document.getElementById("bgHasil");

const batu = document.getElementById("comBatu");
const kertas = document.getElementById("comKertas");
const gunting = document.getElementById("comGunting");

const refreshButton = document.getElementById("refreshButton");
const warningAlert = new bootstrap.Modal(
  document.getElementById("warningAlert"),
  {
    keyboard: false,
  }
);

let refresh = true;

class DataPlayers {
  constructor(playerOne) {
    this.player = playerOne;
  }
  player() {
    return this.player;
  }

  computer() {
    let options = ["kertas", "gunting", "batu"];

    let random = Math.floor(Math.random() * options.length);
    let results = options[random];

    this.#action(results);

    return results;
  }

  #action(results) {
    batu.classList.remove("active");
    kertas.classList.remove("active");
    gunting.classList.remove("active");

    if (results === "kertas") kertas.classList.add("active");
    else if (results === "batu") batu.classList.add("active");
    else gunting.classList.add("active");
  }
}

const Decision = (Data) =>
  class extends Data {
    decision() {
      let hasil = null;
      let player = super.player();
      let computer = super.computer();

      const playerWin = "Player 1 Win";
      const draw = "Draw";
      const comWin = "Com Win";

      if (computer === "gunting" && player === "gunting") hasil = draw;
      else if (computer === "gunting" && player === "kertas") hasil = comWin;
      else if (computer === "gunting" && player === "batu") hasil = playerWin;
      else if (computer === "kertas" && player === "gunting") hasil = playerWin;
      else if (computer === "kertas" && player === "kertas") hasil = draw;
      else if (computer === "kertas" && player === "batu") hasil = comWin;
      else if (computer === "batu" && player === "gunting") hasil = comWin;
      else if (computer === "batu" && player === "kertas") hasil = playerWin;
      else if (computer === "batu" && player === "batu") hasil = draw;
      else hasil = "hasil tidak diketahui!";

      this.#action(player, computer, hasil);
    }

    #action(player, computer, hasil) {
      output.innerHTML = hasil;
      output.classList.add("result-text");
      bgHasil.classList.remove("play-game-vs");
      bgHasil.classList.add("play-game-result");

      if (hasil === "Draw") bgHasil.classList.add("draw");
      else bgHasil.classList.remove("draw");

      console.log(`player : ${player}`);
      console.log(`computer : ${computer}`);
      console.log(`decision results: ${hasil}`);
    }
  };

class Main extends Decision(DataPlayers) {
  constructor(props) {
    super(props);
  }

  result() {
    super.decision();
  }
}

function testPlay (){
  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      let dataPlayer = this.dataset.player;

      let main = new Main(dataPlayer);

      if (refresh) {
        refresh = false;
        main.result();
      } else {
        console.log("refresh dulu dong");
        warningAlert.show();
      }
    });
  });
}

// buttons.forEach((button) => {
//   button.addEventListener("click", function () {
//     let dataPlayer = this.dataset.player;

//     let main = new Main(dataPlayer);

//     if (refresh) {
//       refresh = false;
//       main.result();
//     } else {
//       console.log("refresh dulu dong");
//       warningAlert.show();
//     }
//   });
// });

refreshButton.addEventListener("click", function () {
  this.classList.toggle("refresh-rotate");
  batu.classList.remove("active");
  kertas.classList.remove("active");
  gunting.classList.remove("active");

  output.innerHTML = "VS";
  output.classList.remove("result-text");
  bgHasil.classList.add("play-game-vs");
  bgHasil.classList.remove("play-game-result");

  refresh = true;
});
