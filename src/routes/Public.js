import {
  Welcome,
  Login,
  Register,
  Games,
  GameDetail
} from '../pages'
const routes = [
  {
    id: 0,
    key: "welcome",
    name: "welcome",
    element: <Welcome />,
    path: "/"
  },
  {
    id: 1,
    key: "login",
    name: "login",
    element: <Login />,
    path: "/login",
  },
  {
    id: 2,
    key: "register",
    name: "register",
    element: <Register />,
    path: "/register"
  },
  {
    id: 3,
    key: "games",
    name: "games",
    element: <Games />,
    path: "/games"
  },
  {
    id: 4,
    key: "games-detail",
    name: "games-detail",
    element: <GameDetail />,
    path: "/games/:id"
  }
];

export default routes;