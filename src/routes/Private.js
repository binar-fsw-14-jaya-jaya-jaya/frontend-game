import {
  RockPaperScissors,
  AdminDashboard,
  EditProfil,
  Welcome,
  Games,
  GameDetail
} from "../pages";

const routes = [
  {
    id: 0,
    key: "welcome",
    name: "welcome",
    element: <Welcome />,
    path: "/"
  },
  {
    id: 1,
    key: "dashboard",
    name: "dashboard",
    element: <AdminDashboard />,
    path: "/admin/dashboard",
  },
  {
    id: 2,
    key: "edit-profil",
    name: "edit-profil",
    element: <EditProfil />,
    path: "/admin/profil/:id/edit",
  },
  {
    id: 3,
    key: "rock-paper-scissors",
    name: "rock-paper-scissors",
    element: <RockPaperScissors />,
    path: "/games/rock-paper-scissors/play",
  },
  {
    id: 4,
    key: "games",
    name: "games",
    element: <Games />,
    path: "/games"
  },
  {
    id: 5,
    key: "games-detail",
    name: "games-detail",
    element: <GameDetail />,
    path: "/games/:id"
  }
];

export default routes;
