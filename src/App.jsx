
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Public, Private } from "./routes";
import { getToken } from "./utils/cookie";

function App() {
  const token = getToken();

  return (
    <BrowserRouter>
      <Routes>
          {!token
            ? Public.map(({ key, path, element}) =>(
              <Route key={key} path={path} element={element} end></Route>
            ))
            : Private.map(({ key, path, element }) => (
              <Route key={key} path={path} element={element} end></Route>
            ))
          }
      </Routes>
    </BrowserRouter>

  );
}

export default App;
