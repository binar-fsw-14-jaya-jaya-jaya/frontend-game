import React from "react";
import { Nav, Navbar, Container, Image, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getToken, removeToken } from "../utils/cookie";

function Navigation() {
  const token = getToken();

  const logout = () => {
    removeToken();
    window.location.href ="/";
  }

  return (
    <Navbar expand="lg" variant="dark" className="navbar-dark-transparent">
      <Container>
        <Navbar.Brand href="#" className="navbar-brand mx-5">
          <Image src="/assets/images/logo.svg"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto mb-2 mb-lg-0">
            <Nav.Link className="text-white mx-3" href="/" aria-current="page">HOME</Nav.Link>
            <Nav.Link className="text-white mx-3" href="/games">GAMES</Nav.Link>
            <Nav.Link className="text-white mx-3" href="#">CONTACT</Nav.Link>
            <Nav.Link className="text-white mx-3" href="#">ABOUT ME</Nav.Link>
          </Nav>
          <div className="d-flex">
            <Nav className="me-auto mb-2 mb-lg-0">
              <Dropdown className="mx-3 border-right">
                <Dropdown.Toggle
                  className="bg-transparent border-0 ps-0"
                  id="dropdown-basic"
                  style={{outline: 'none', boxShadow: 'none' }}>
                  AKUN
                </Dropdown.Toggle>
                <Dropdown.Menu className="dropdown-menu-dark">
                  {
                    !token
                      ? (
                        <>
                          <Dropdown.Item href="/login">LOGIN</Dropdown.Item>
                          <Dropdown.Item href="/register">REGISTER</Dropdown.Item>
                        </>
                      )
                      : (
                        <>
                          <Dropdown.Item href="/admin/dashboard">DASHBOARD</Dropdown.Item>
                          <Dropdown.Item href="#" onClick={logout}>LOGOUT</Dropdown.Item>
                        </>
                      )
                  }
                </Dropdown.Menu>
              </Dropdown>
            </Nav>

            <Link className="mx-3" to="#" style={{ textDecoration: 'none' }}>
              <Image src="/assets/images/xbox.svg" className="img-fluid" />
            </Link>
            <Link className="mx-3" to="#" style={{ textDecoration: 'none' }}>
              <Image src="/assets/images/steam.svg" className="img-fluid" />
            </Link>
          </div>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Navigation