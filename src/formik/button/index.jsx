import * as React from "react";

export default function Button({
  children,
  onClick,
  type,
  id,
  loading
}) {
  return (
    <>
      <button
        id={id}
        type={type}
        onClick={onClick}
        className="btn btn-xl"
      >
        {loading ? (
          <>
             <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
             Loading...
          </>
        ) : (
          children
        )}
      </button>
    </>
  )
}