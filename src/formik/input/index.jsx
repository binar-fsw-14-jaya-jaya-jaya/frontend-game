import * as React from "react";

function Input({
  placeholder,
  type,
  id,
  name,
  onChange = () => {},
  onBlur = () => {},
  isSelect = false,
  onClick = () => {},
  error,
  errorMsg,
  value,
  label
}) {

  const inputRef = React.useRef();

  const onBeforeClick = () => {
    if (!isSelect) {
      inputRef.current.focus();
    } else {
      inputRef.current.blur();
    }
    onClick();
  };

  const valueProps = value !== undefined ? { value } : {};

  return (
    <div className="form-floating mb-3">
      <input
        className="form-control"
        id={id}
        ref={inputRef}
        name={name}
        type={type}
        onChange={(e) => {
          e.persist();
          onChange(e, e.target.value);
        }}
        onClick={onBeforeClick}
        onKeyUp={onBlur}
        placeholder={placeholder}
        autoComplete="off"/>
        <label>{ label }</label>
      { error && <span className="text-danger">{errorMsg}</span> }
    </div>
  )
}

export default Input