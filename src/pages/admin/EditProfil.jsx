import React, { Component } from "react";
import { Container, Row } from 'react-bootstrap';
import Navigation from "../../components/Navigation";
import Footer from "../../components/Footer";

class EditProfil extends Component
{
  render() {
    return(
      <div className="bg-black">
        <Navigation />
        <Container>
          <Row>
            <div className="col-lg-8 offset-lg-2 py-5" style={{ paddingTop: '8rem !important' }}>
              <div className="card" style={{ background: 'rgba(112, 108, 104, 0.5)', borderRadius: '15px' }}>
                <div className="card-title text-center py-3">
                  <h1 className="fw-bold text-uppercase text-white" style={{ letterSpacing: '3px' }}>
                    USER PROFIL
                  </h1>
                </div>
                <div className="card-body">
                  <form action="" method="post">
                    <input nameName="_method" type="hidden" value="PUT" />

                    <div className="form-floating mb-3">
                      <input className="form-control" id="username" type="text" name="username" value="" placeholder="username"
                        data-sb-validations="required" autoComplete="off" autofocus />
                      <label for="username">Your username account</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input className="form-control" id="email" type="email" name="email" value="" placeholder="username@email.com"
                        data-sb-validations="required" autoComplete="off"  />
                      <label for="email">Your email addres</label>
                    </div>

                    <div className="form-floating mb-3">
                      <input className="form-control" id="password" type="password" name="password" placeholder="******" autoComplete="off" />
                      <label for="password">Your password</label>
                    </div>

                    <span className="text-muted">BIODATA</span>
                    <div className="form-floating mb-3">
                      <input className="form-control" id="country" type="text" name="country" value="" placeholder="example: Indonesia"
                        data-sb-validations="required" autoComplete="off" />
                      <label for="country">Your country</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input className="form-control" id="age" type="number" name="age" value="" placeholder="example: 25"
                        data-sb-validations="required" autoComplete="off"  />
                      <label for="age">Your age</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input className="form-control" id="bio" type="text" name="bio" value="" placeholder="example: 'One of my highlights of the year.'"
                        data-sb-validations="required" autoComplete="off" />
                      <label for="bio">Your Bio</label>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-xl">SUBMIT</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </Row>
        </Container>
        <Footer />
      </div>
    )
  }
}

export default EditProfil