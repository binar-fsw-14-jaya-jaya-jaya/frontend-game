import React from "react";
import { Container } from "react-bootstrap";

const Header = () => {
  return (
    <>
    <header className="py-base main-bg" id="home">
      <Container className="px-4 px-lg-5 h-100">
        <div
          className="
            row
            gx-lg-5
            h-100
            align-items-center
            justify-content-center
            text-center
          "
        >
          <div className="col-lg-12 col-md-12 col-sm-12 top-50 start-50 py-5">
            <h1 className="main-bg-title">Play Traditional Game</h1>

            <h3 className="text-white text-capitalize fw-light main-bg-subtitle">
              Experience New Traditional game
            </h3>
            <div className="text-center text-uppercase">
              <a className="btn btn-xl" href="/dashboard">Play Now</a>
            </div>
          </div>

          <div
            className="
              col-lg-12 col-md-12
              align-self-end
              top-50
              start-50
              p-2
              text-white
            "
          >
            <div className="main-bg-the-story">The Story</div>
            <svg
              className="icon-the-story"
              viewBox="0 0 23 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M21.5786 1.83362H1.32422L11.338 10.0027L21.5786 1.83362Z"
                stroke="white"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
        </div>
      </Container>
    </header>
    </>
  )
}

export default Header
