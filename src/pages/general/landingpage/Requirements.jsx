import React from "react";
import { Container } from "react-bootstrap";

const Requirements = () => {
  return (
    <>
      <section className="py-base requirements">
        <Container className="px-4 px-lg-5 h-100">
          <div className="row">
            <div className="col-lg-12 col-sm-12 text-center p-5">
              <h6 className="text-capitalize text-white fw-light">
                Can my Computer run this Game?
              </h6>
            </div>
          </div>
          <div className="row gx-lg-5 h-100">
            <div className="col-lg-6 col-sm-12">
              <div className="requirements-title">
                <span className="text-uppercase text-white">System</span><br />
                <span className="text-uppercase text-white">Requirements</span>
              </div>

              <table className="table table-bordered requirements-table mt-5">
                <tbody>
                  <tr>
                    <td>
                      <h2 className="table-requirements-title">OS:</h2>
                      <p className="text-white">
                        Windows 7 64-bit only<br />(No OSX support at this time)
                      </p>
                    </td>
                    <td>
                      <h2 className="table-requirements-title">Processor:</h2>
                      <p className="text-white">
                        Intel Core 2 Duo @2.4GHz or AMD Athlon X2 @ 2.8GHz
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h2 className="table-requirements-title">Memory:</h2>
                      <p className="text-white">4 GB RAM</p>
                    </td>
                    <td>
                      <h2 className="table-requirements-title">Storage:</h2>
                      <p className="text-white">8 GB available space</p>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <h2 className="table-requirements-title">Graphics:</h2>
                      <p className="text-white">
                        NVIDIA GeForce GTX 660 2GB or<br />AMD Radeon HD 7850
                        2GB DirectX11 (Shader Model S)
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </Container>
      </section>
    </>
  )
}

export default Requirements