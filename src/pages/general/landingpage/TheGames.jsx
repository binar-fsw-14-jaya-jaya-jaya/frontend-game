import React, { useRef} from "react";
import { Carousel } from "react-bootstrap";

const TheGames = () => {

  const ref = useRef(null);

  const onPrevClick = () => {
    ref.current.prev();
  };
  const onNextClick = () => {
    ref.current.next();
  };

  return (
    <>
    <section className="py-base the-games">
      <div className="container px-4 px-lg-5 h-100">
        <div className="row gx-lg-5">
          <div
            className="col-lg-4 text-white justify-content-end py-lg-5 px-lg-0"
          >
            <h3 className="the-games-subtitle text-start">What's so special?</h3>
            <h1 className="the-games-title">The Games</h1>
          </div>
          <div className="col-lg-8 px-0 py-lg-5">
            <div className="d-flex justify-content-center">
                <div className="carousel-button">
                  <button
                    className="btn shadow-none p-0"
                    type="button"
                    onClick={ onPrevClick }
                  >
                    <span aria-hidden="true"
                      ><svg
                        width="41"
                        height="19"
                        viewBox="0 0 20 38"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18 1.70007L18 35.7001L2 18.8904L18 1.70007Z"
                          stroke="white"
                          strokeWidth="3.36454"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </span>
                    <span className="visually-hidden">Previous</span>
                  </button>
                </div>
                <Carousel controls={false} ref={ref}>
                  <Carousel.Item interval={1000}>
                    <img
                      className="d-block w-100"
                      src="../assets/images/rockpaperstrategy-1600.jpg"
                      alt="First slide"
                    />
                  </Carousel.Item>
                  <Carousel.Item interval={1000}>
                    <img
                      className="d-block w-100"
                      src="../assets/images/rockpaperstrategy-1600.jpg"
                      alt="Second slide"
                    />
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="../assets/images/rockpaperstrategy-1600.jpg"
                      alt="Third slide"
                    />
                  </Carousel.Item>
                </Carousel>
                <div className="carousel-button">
                  <button
                    className="btn shadow-none p-0"
                    style={{outline: 'none'}}
                    type="button"
                    onClick={ onNextClick }
                  >
                    <span aria-hidden="true"
                      ><svg
                        width="41"
                        height="19"
                        viewBox="0 0 18 39"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1.82153 2.65635L1.82153 36.7297L15.5642 19.8838L1.82153 2.65635Z"
                          stroke="white"
                          strokeWidth="3.36454"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </span>
                    <span className="visually-hidden">Next</span>
                  </button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    </>
  )
}

export default TheGames;