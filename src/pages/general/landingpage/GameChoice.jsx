import React from "react";
import { Container, Row, Accordion } from "react-bootstrap";

const GameChoice = () => {
  return (
    <>
      <section className="py-base game-choice">
        <Container className="p-5 my-5 h-100">
          <Row>
            <div className="col-lg-5 offset-lg-7 col-md-12">
              <div className="text-white" style={{ padding: '1rem 1.25rem' }}>
                <h3 className="game-choice-subtitle">What's so special?</h3>
                <h1 className="game-choice-title">Features</h1>
              </div>
              <Accordion defaultActiveKey="0" flush>
                <Accordion.Item eventKey="0">
                  <Accordion.Header>
                    <span className="accordion-indicator deactive"></span>
                    TRADITIONAL GAME
                  </Accordion.Header>
                  <Accordion.Body className="accordion-line">
                    <p>
                      If you miss your childhood, we provide <br />
                      many traditional games here
                    </p>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header>
                    <span className="accordion-indicator deactive"></span>
                    GAMES SUIT
                  </Accordion.Header>
                  <Accordion.Body className="accordion-line">
                    <p>
                      It is a long established fact that a reader will be
                      distracted by the readable content of a page when
                      looking at its layout. The point of using Lorem Ipsum is
                      that it has a more-or-less normal distribution of
                      letters, as opposed to using 'Content here, content
                      here', making it look like readable English.
                    </p>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                  <Accordion.Header>
                    <span className="accordion-indicator deactive"></span>
                    TBD
                  </Accordion.Header>
                  <Accordion.Body className="accordion-line-last">
                    <p>
                      There are many variations of passages of Lorem Ipsum
                      available, but the majority have suffered alteration in
                      some form, by injected humour, or randomised words which
                      don't look even slightly believable.
                    </p>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </div>
          </Row>
        </Container>
      </section>
    </>
  )
}

export default GameChoice
