import React, { useState } from "react";
import { Container, Row, Image } from 'react-bootstrap';
import axios from 'axios';
import { setToken } from "../../../utils/cookie";
import { useFormik } from "formik";
import Input from "../../../formik/input";
import Button from "../../../formik/button";
import * as Yup from "yup";

const loginSchema = Yup.object().shape({
  username: Yup.string().required("Username tidak boleh kosong"),
  password: Yup.string().required("Password tidak boleh kosong")
});

function Login () {
  const [loading, setLoading] = useState(false);

  const onSubmit = async (values) => {
    setLoading(true);

    let formData = {
      username: values.username,
      password: values.password
    };

    axios.post(`${process.env.REACT_APP_API}/api/login`, formData)
      .then( res => {
        setToken(res.data.user.accessToken);
        window.location.href ="/admin/dashboard";
      }).catch(error => {
        console.log(error)
      })
  }

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      onSubmit(values);
    },
  });

  return(
    <div className="bg-black">
      <Container >
        <Row>
          <div className="col-lg-4 offset-lg-4 py-5">
            <div className="card" style={{ background: 'rgba(112, 108, 104, 0.5)', borderRadius: '15px' }}>
              <div className="card-title text-center py-3">
                <div className="text-center">
                  <a href="/">
                    <Image src="/assets/images/logo.svg" className="w-25 h-25 m-3"/>
                  </a>
                </div>
                <h1 className="fw-bold text-uppercase text-white" style={{ letterSpacing: '3px' }}>
                  Login
                </h1>

              </div>
              <div className="card-body">
                <form onSubmit={formik.handleSubmit}>

                    <Input
                      id="username-input"
                      name="username"
                      placeholder="Masukkan username"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={
                        formik.errors.username !== undefined && formik.touched.username
                      }
                      label="Your username"
                      errorMsg={formik.errors.username}
                    />

                    <Input
                      type="password"
                      id="password-input"
                      name="password"
                      placeholder="Password"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={
                        formik.errors.password !== undefined &&
                        formik.touched.password
                      }
                      label="Your password"
                      errorMsg={formik.errors.password}
                    />

                    <p className="my-2 text-white">
                      Don't have an account yet?
                      <a href="/register" className="text-decoration-none">
                        Let's register</a>
                    </p>

                  <div className="text-center">
                    <Button id="login-submit" type="submit" loading={loading}>
                      Login
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  )
}

export default Login