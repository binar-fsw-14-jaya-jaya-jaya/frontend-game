import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Container, Row, Image, Table  } from "react-bootstrap";
import Navigation from "../../../components/Navigation";
import Footer from "../../../components/Footer";

class GameDetail extends Component
{
  render() {
    return(
      <div className="bg-black">
        <Navigation />
          <Container>
            <Row>
              <div className="col-lg-12 mb-3">
                <h3 className="fw-bold text-orange">Name Game Detail</h3>
              </div>
            </Row>

            <Row className="pb-3">
              <div className="col-lg-4">
                <div className="card border-0 bg-black" style={{ width: '20rem' }}>
                  <Image src="https://via.placeholder.com/1200" className="card-img-top" style={{ borderRadius: '15px' }}/>
                  <div className="card-body">
                    <div className="d-grid gap-2">
                        <Link to="/games/rock-paper-scissors/play" className="btn btn-primary">Play Now</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-8">
                <Table className="table table-dark table-striped">
                  <tbody>
                    <tr>
                      <th>Genre</th>
                      <td>Genre Name</td>
                    </tr>
                    <tr>
                      <th>Date Rilis</th>
                      <td>02/11/2021</td>
                    </tr>
                    <tr>
                      <th>Total Play</th>
                      <td>3000 play</td>
                    </tr>
                    <tr>
                      <th>Descriptions</th>
                      <td>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Repudiandae quisquam itaque amet eos commodi libero vero,
                        perferendis expedita unde ipsum id cumque distinctio nostrum,
                        alias earum error et blanditiis fugit.
                      </td>
                    </tr>
                    <tr>
                      <th>Production By</th>
                      <td>Universal Studio</td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Row>

            <Row>
            <div className="col-lg-12 pt-3 text-center">
              <h3 className="fw-bold text-orange">Leaderboard</h3>
            </div>
          </Row>

          <Row>
            <Table className="table table-success table-striped">
              <thead>
                <tr>
                  <th scope="col" style={{ width: '5%' }}>Rangking</th>
                  <th scope="col" style={{ width: '50%' }}>Name</th>
                  <th scope="col" style={{ width: '10%' }}>Total Score</th>
                  <th scope="col" style={{ width: '30%' }}>Country</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>100</td>
                  <td>Thailand</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>80</td>
                  <td>Vietnam</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>60</td>
                  <td>Indonesia</td>
                </tr>
              </tbody>
            </Table>
          </Row>
          </Container>
        <Footer />
      </div>
    )
  }
}

export default GameDetail