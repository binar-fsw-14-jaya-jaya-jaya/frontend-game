import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Navigation from "../../../components/Navigation";
import Footer from "../../../components/Footer";
import { Container, Row, Image,  } from "react-bootstrap";

class Games extends Component
{
  render() {
    return(
      <div className="bg-black">
        <Navigation />
        <Container>
          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">Popular</h3>
            </div>
          </Row>

          <Row>
            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

          </Row>

          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">New</h3>
            </div>
          </Row>

          <Row>
            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

          </Row>

          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">Sport</h3>
            </div>
          </Row>

          <Row>
            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image src="https://via.placeholder.com/150" className="card-img-top" style={{ borderRadius: '15px' }}/>
                <div className="card-body">
                  <Link to="/games/1" className="text-decoration-none">
                    <h5 className="card-title text-white">Card title</h5>
                  </Link>
                </div>
              </div>
            </div>

          </Row>

        </Container>
        <Footer />
      </div>
    )
  }
}

export default Games