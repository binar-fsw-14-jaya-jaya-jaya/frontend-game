import React  from "react";
import '../../../assets/css/additional_2.css';
import { Container, Row, Image } from 'react-bootstrap';
import { Link } from "react-router-dom";
// import { Switch, Route } from "react-router-dom";
import Play from "../../../components/RockPaperScissors/Play";
// import Rules from "../../../components/RockPaperScissors/Rules";


function RockPaperScissors()
{
  // render(){
    // const [myChoice, setMyChoice] = useState("");
    // console.log(game(myChoice));
    // const [score, setScore ] = useState(0);

    // console.log(myChoice);
    return(
      <div className="bg-brow">
        <Container>
        <Row>
            <div className="col-lg-12 col-md-12 py-3">
              <Row>
                <div
                  className="col-lg-1 col-md-1 d-flex align-items-center justify-content-center my-2"
              >
                  <Link to="/" title="back">
                    <svg
                      width="36"
                      height="38"
                    viewBox="0 0 36 38"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M35.0039 37.8633L0.09375 22.4648V17.4375L35.0039 0V8.4375L10.957 19.582L35.0039 29.4609V37.8633Z"
                      fill="#724C21"
                    />
                  </svg>
                </Link>
              </div>
              <div className="col-lg-1 col-md-1 my-2 d-flex align-items-center justify-content-center">
                <Image
                  src="/assets/images/play_game/logo-1.png"
                />
              </div>
              <div className="col-lg-10 col-md-10 my-2 d-flex align-items-center title-game ">
                <span className="play-game-title">ROCK PAPER SCISSORS</span>
              </div>
            </Row>
          </div>
        </Row>
        {/* <Switch>
          <Route exact path="/games/rock-paper-scissors/play"> */}
            <Play />
          {/* </Route> */}
          {/* <Route path="/games/rock-paper-scissors/rules">
            <Rules myChoice={myChoice} />
          </Route> */}
        {/* </Switch> */}

        </Container>
      </div>

      // <div className="bg-brow">
      //   <Container>
      //     <Row>
      //       <div className="col-lg-12 col-md-12 py-3">
      //         <Row>
      //           <div
      //             className="col-lg-1 col-md-1 d-flex align-items-center justify-content-center my-2"
      //           >
      //             <Link to="/" title="back">
      //               <svg
      //                 width="36"
      //                 height="38"
      //                 viewBox="0 0 36 38"
      //                 fill="none"
      //                 xmlns="http://www.w3.org/2000/svg"
      //               >
      //                 <path
      //                   d="M35.0039 37.8633L0.09375 22.4648V17.4375L35.0039 0V8.4375L10.957 19.582L35.0039 29.4609V37.8633Z"
      //                   fill="#724C21"
      //                 />
      //               </svg>
      //             </Link>
      //           </div>
      //           <div className="col-lg-1 col-md-1 my-2 d-flex align-items-center justify-content-center">
      //             <Image
      //               src="/assets/images/play_game/logo-1.png"
      //             />
      //           </div>
      //           <div className="col-lg-10 col-md-10 my-2 d-flex align-items-center title-game ">
      //             <span className="play-game-title">ROCK PAPER SCISSORS</span>
      //           </div>
      //         </Row>
      //       </div>
      //     </Row>

      //     <Row className="pt-5">
      //       <div className="col-lg-4 col-md-12 d-flex justify-content-center">
      //         <Row className="g-2 player-satu" id="playerId">
      //           <div className="col-12">
      //             <div className="p-3 text-center play-game-subtitle">PLAYER 1</div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <button
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 title="batu"
      //                 data-player="batu"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/batu.png"
      //                 />
      //               </button>
      //             </div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <button
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 title="kertas"
      //                 data-player="kertas"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/kertas.png"
      //                 />
      //               </button>
      //             </div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <button
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 title="gunting"
      //                 data-player="gunting"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/gunting.png"
      //                 />
      //               </button>
      //             </div>
      //           </div>
      //         </Row>
      //       </div>
      //       <div className="col-lg-4 col-md-12 d-flex justify-content-center align-items-center">
      //         <Row>
      //           <div className="col-lg-12">
      //             <div
      //               className="
      //                 p-3
      //                 play-game-vs
      //                 d-flex
      //                 justify-content-center
      //                 align-items-center
      //               "
      //               id="bgHasil"
      //             >
      //               <span id="idHasil">VS</span>
      //             </div>
      //           </div>
      //         </Row>
      //       </div>
      //       <div className="col-lg-4 col-md-12 d-flex justify-content-center">
      //         <Row className="g-2" id="computerId">
      //           <div className="col-lg-12">
      //             <div className="p-3 text-center play-game-subtitle">COM</div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <div
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 id="comBatu"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/batu.png"
      //                 />
      //               </div>
      //             </div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <div
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 id="comKertas"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/kertas.png"
      //                 />
      //               </div>
      //             </div>
      //           </div>
      //           <div className="col-12 text-center">
      //             <div className="py-4 d-flex justify-content-center">
      //               <div
      //                 className="
      //                   btn btn-play-game
      //                   d-flex
      //                   justify-content-center
      //                   align-items-center
      //                 "
      //                 id="comGunting"
      //               >
      //                 <Image
      //                   className="play-game-icon-size"
      //                   src="/assets/images/play_game/gunting.png"
      //                 />
      //               </div>
      //             </div>
      //           </div>
      //         </Row>
      //       </div>
      //     </Row>

      //     <Row className="pb-5">
      //       <div className="col-lg-12 col-md-12 d-flex align-items-center justify-content-center">
      //         <div className="pe-auto refresh" id="refreshButton">
      //           <Image
      //             src="/assets/images/play_game/refresh.png"
      //             alt="Refresh"
      //             title="refresh"
      //           />
      //         </div>
      //       </div>
      //     </Row>
      //   </Container>

      //   {/* <script src="/assets/js/play_game.js"/> */}
      //   {/* <Helmet>
      //     <script src="/assets/js/play_game.js" type="text/javascript" />
      //   </Helmet> */}
      //   {/* <ScriptTag type="text/javascript" src="/assets/js/play_game.js" /> */}

      // </div>
    );
  }
// }

export default RockPaperScissors