import React, { Component } from "react";
import Navigation from "../../components/Navigation";
import Header from "./landingpage/Header";
import TheGames from './landingpage/TheGames';
import GameChoice from "./landingpage/GameChoice";
import Requirements from "./landingpage/Requirements";
import TopScore from './landingpage/TopScore';
import Subscribe from "./landingpage/Subscribe";
import Footer from "../../components/Footer";

class Welcome extends Component
{

  render() {
    return(
      <>
      <Navigation />
      <Header />
      <main className="bg-black">
        <TheGames />
        <GameChoice />
        <Requirements />
        <TopScore />
        <Subscribe />
      </main>
      <Footer />
      </>
    )
  }
}

export default Welcome