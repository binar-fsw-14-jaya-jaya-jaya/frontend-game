import Welcome from "./general/Welcome";
import Login from "./general/auth/Login";
import Register from "./general/auth/Register";
import Games from "./general/games/List";
import GameDetail from "./general/games/Detail";
import RockPaperScissors from "./general/games/RockPaperScissors";

import AdminDashboard from "./admin/Dashboard";
import EditProfil from "./admin/EditProfil";

export {
  Welcome,
  Login,
  Register,
  Games,
  GameDetail,
  RockPaperScissors,
  AdminDashboard,
  EditProfil
}